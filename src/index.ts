/*
* Task 1: Times
*/
function times(array, numberOfTimes) {
    var result = [];
    var i = 0
    if (numberOfTimes == null) {
        numberOfTimes = 2;
    }
    while (i < numberOfTimes) {
        result = result.concat(array);
        i += 1;
    }

    return result;
}

/*
* Task 2: Logger
*/
function logger(data, serviceInfo) {
    if (serviceInfo == null) {
        serviceInfo = {serviceName: 'global', serviceId: 1};
    }

    const result = {}
    for (var i in data) {
        const key = `${serviceInfo['serviceId']}-${i}`;
        result[key] = `[${serviceInfo['serviceName']}] ${data[i]}`;
    }

    return result;
}

/*
* Task 3: Array NTH Element
*/
function everyNth(arr, nth) {
    const result = [];
    if (nth == null) {
        nth = 1;
    }
    var i = 0 + nth - 1;
    while (i < arr.length) {
        result.push(arr[i]);
        i += nth;
    }

    return result;
}
export {times, logger, everyNth}
